const mongoose = require('mongoose');
const redis = require('redis');
const util = require('util');
const requireLogin = require('../middlewares/requireLogin');

const Blog = mongoose.model('Blog');

module.exports = app => {
  app.get('/api/blogs/:id', requireLogin, async (req, res) => {
    const blog = await Blog.findOne({
      _user: req.user.id,
      _id: req.params.id,
    });

    res.send(blog);
  });

  app.get('/api/blogs', requireLogin, async (req, res) => {
    const redisUrl = 'redis://127.0.0.1:6379';
    const client = redis.createClient(redisUrl);
    // pass a reference to the client.get() function
    // ...to promisify
    // ...so that it returns a promise
    // ...instead of callback
    // It returns a new function and we overwrite
    // ...client.get() with the returned function
    client.get = util.promisify(client.get);
    // Check if query exists in redis

    // await this promise to be resolved
    const cachedBlogs = await client.get(req.user.id);

    if (cachedBlogs) {
      console.log('SERVING FROM CACHE...');
      return res.send(JSON.parse(cachedBlogs));
    }

    // we do NOT have cached blogs
    const blogs = await Blog.find({_user: req.user.id});
    console.log('SERVING FROM MONGODB');
    res.send(blogs);
    // update cache
    client.set(req.user.id, JSON.stringify(blogs));
  });

  app.post('/api/blogs', requireLogin, async (req, res) => {
    const {title, content} = req.body;

    const blog = new Blog({
      title,
      content,
      _user: req.user.id,
    });

    try {
      await blog.save();
      res.send(blog);
    } catch (err) {
      res.send(400, err);
    }
  });
};
