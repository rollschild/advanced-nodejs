const mongoose = require('mongoose');
const redis = require('redis');
const util = require('util');
const keys = require('../config/keys');

// const redisUrl = 'redis://127.0.0.1:6379';
const client = redis.createClient(keys.redisUrl);
client.hget = util.promisify(client.hget);
const exec = mongoose.Query.prototype.exec;

mongoose.Query.prototype.cache = function(options = {}) {
  this.useCache = true;
  this.hashKey = JSON.stringify(options.key || ''); // number or string
  return this; // make cache() function chainable..
};

mongoose.Query.prototype.exec = async function() {
  // console.log(this.getQuery());
  // console.log(this.mongooseCollection.name);
  if (!this.useCache) {
    return exec.apply(this, arguments);
  }

  const key = JSON.stringify(
    Object.assign({}, this.getQuery(), {
      collection: this.mongooseCollection.name,
    }),
  );

  // Check if key exists in redis
  const cacheValue = await client.hget(this.hashKey, key);
  if (cacheValue) {
    // actually an array of objects
    // const doc = new this.model(JSON.parse(cacheValue));
    const doc = JSON.parse(cacheValue);
    return Array.isArray(doc)
      ? doc.map(d => new this.model(d))
      : new this.model(doc);
    return doc;
  } else {
    // It returns a model instance
    // ...or, a mongoose document
    const result = await exec.apply(this, arguments);
    // console.log(result);

    // the duration will only apply to future caches
    client.hset(this.hashKey, key, JSON.stringify(result), 'EX', 10);
    return result;
  }
};

module.exports = {
  clearHash(hashKey) {
    client.del(JSON.stringify(hashKey));
  },
};
