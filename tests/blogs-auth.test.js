const Page = require('./helpers/page');

let page;

beforeEach(async () => {
  page = await Page.build();
  await page.goto('http://localhost:3000');
});

afterEach(async () => {
  await page.close();
});

describe('When logged in, ', async () => {
  // common setup for tests when logged in..
  beforeEach(async () => {
    await page.login();
    await page.click('a.btn-floating');
  });

  test('A form to create a blog is shown', async () => {
    // await page.goto('localhost:3000/blogs');
    /* DRY */
    const label = await page.getContentsOf('form label');
    expect(label).toEqual('Blog Title');
  });

  describe('...And using invalid inputs', async () => {
    beforeEach(async () => {
      await page.click('form button');
    });

    test('The form shows an error message', async () => {
      const titleError = await page.getContentsOf('.title .red-text');
      const contentError = await page.getContentsOf('.content .red-text');
      expect(titleError).toEqual('You must provide a value');
      expect(contentError).toEqual('You must provide a value');
    });
  });

  describe('...And using VALID inputs', async () => {
    beforeEach(async () => {
      await page.type('.title input', 'Test Title');
      await page.type('.content input', 'Test Content');
      await page.click('form button');
    });
    test('Submitting takes user to review page', async () => {
      const text = await page.getContentsOf('h5');
      expect(text).toEqual('Please confirm your entries');
    });

    test('Submitting then saving will add blog to index page', async () => {
      await page.click('button.green');
      await page.waitFor('.card');
      const title = await page.getContentsOf('.card-content .card-title');
      const content = await page.getContentsOf('.card-content p');
      expect(title).toEqual('Test Title');
      expect(content).toEqual('Test Content');
    });
  });
});
