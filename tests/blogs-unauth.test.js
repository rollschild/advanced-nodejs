const Page = require('./helpers/page');

let page;

beforeEach(async () => {
  page = await Page.build();
  await page.goto('http://localhost:3000');
});

afterEach(async () => {
  await page.close();
});

describe('NOT logged in...', async () => {
  // don't log in
  process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
  });

  const actions = [
    {
      method: 'get',
      path: '/api/blogs',
    },
    {
      method: 'post',
      path: '/api/blogs',
      data: {
        title: 'It is a title',
        content: "There's some content...",
      },
    },
  ];

  test('Blog related actions are prohibited', async () => {
    const results = await page.executeReqs(actions);

    for (let result of results) {
      expect(result).toEqual({error: 'You must log in!'});
    }
  });
});
