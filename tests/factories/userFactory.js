const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = () => {
  // this is an async operation
  return new User({}).save();
}
