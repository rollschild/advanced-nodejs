const Page = require('./helpers/page');

let page;

beforeEach(async () => {
  page = await Page.build();
  await page.goto('http://localhost:3000');
});

afterEach(async () => {
  await page.close();
});

test('Header with correct text..', async () => {
  const text = await page.getContentsOf('a.brand-logo');
  expect(text).toEqual('Blogster');
});

test('Login link works..', async () => {
  await page.click('.right a');
  const url = await page.url();
  // console.log(url);
  expect(url).toMatch(/accounts\.google\.com/);
});

// Login
test('When signed in, logout button is shown instead of login', async () => {
  // We will use userFactory to create user
  // ...instead of the line below:
  // const id = '5c18030e7dc694239f2ad646';
  await page.login();
  const text = await page.getContentsOf('a[href="/auth/logout"]');
  expect(text).toEqual('Logout');
});
