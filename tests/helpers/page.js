const puppeteer = require('puppeteer');
const userFactory = require('../factories/userFactory');
const sessionFactory = require('../factories/sessionFactory');

class CustomPage {
  static async build() {
    // generate new puppeteer page
    // create a new instance of CustomPage
    // combine the two together using Proxy
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox'],
    });

    const page = await browser.newPage();
    const customPage = new CustomPage(page);

    return new Proxy(customPage, {
      get: function handler(target, property) {
        return customPage[property] || browser[property] || page[property];
        // we will not need to interact with browser object
        // ...ever again
      },
    });
  }

  constructor(page) {
    this.page = page;
  }

  async login() {
    const user = await userFactory();
    const {session, sig} = sessionFactory(user);

    await this.page.setCookie({name: 'session', value: session});
    await this.page.setCookie({name: 'session.sig', value: sig});
    await this.page.goto('http://localhost:3000/blogs');
    await this.page.waitFor('a[href="/auth/logout"]');
  }

  async getContentsOf(selector) {
    // aim to replace $eval
    return this.page.$eval(selector, ele => ele.innerHTML);
  }

  get(path) {
    return this.page.evaluate(_path => {
      return fetch(_path, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(res => res.json())
        .catch(() => {});
    }, path);
  }

  post(path, data) {
    return this.page.evaluate(
      (_path, _data) => {
        return fetch(_path, {
          method: 'POST',
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(_data),
        })
          .then(res => res.json())
          .catch(() => {});
      },
      path,
      data,
    );
  }

  executeReqs(actions) {
    return Promise.all(
      // will wait for the array of returned promises
      // ...to be ALL resolved
      // ...and return a single Promise
      actions.map(({method, path, data}) => {
        return this[method](path, data);
        // return an array of Promises
      }),
    );
  }
}

module.exports = CustomPage;
// CustomPage.build();
